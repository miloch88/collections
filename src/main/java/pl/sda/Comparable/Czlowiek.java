package pl.sda.Comparable;

import static java.lang.Integer.*;

public class Czlowiek implements Comparable {

    private String imie;
    private String nazwisko;
    private Plec plec;

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public Plec getPlec() {
        return plec;
    }

    public void setPlec(Plec plec) {
        this.plec = plec;
    }

    public int getWiek() {
        return wiek;
    }

    public void setWiek(int wiek) {
        this.wiek = wiek;
    }

    private int wiek;

    public Czlowiek(String imie, String nazwisko, Plec plec, int wiek) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.plec = plec;
        this.wiek = wiek;
    }

    @Override
    public int compareTo(Object o) {
        Czlowiek that = (Czlowiek) o;

//        Porównanie Stringów
//        return imie.compareTo(that.imie);

//        Porównanie intów
        return wiek - that.wiek;
    }

    @Override
    public String toString() {
        return String.format("%s : %s %s, wiek: %d", plec, imie, nazwisko, wiek);
    }
}
