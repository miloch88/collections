package pl.sda.Comparable;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        Czlowiek adam = new Czlowiek("Adam", "Kowalski", Plec.MEZCZYZNA, 45);
        Czlowiek malgorzata = new Czlowiek("Agnieszka", "Adamowicz", Plec.KOBIETA, 150);
        Czlowiek basia = new Czlowiek("Basia", "Zygmuntowska", Plec.KOBIETA, 60);
        Czlowiek kamil = new Czlowiek("Kamil", "Kamilowski", Plec.MEZCZYZNA, 25);

        Czlowiek[] ludziki = new Czlowiek[]{adam, malgorzata, basia, kamil};
        for (Czlowiek cz : ludziki) {
            System.out.println(cz);
        }
        System.out.println();

        Arrays.sort(ludziki);
        for (Czlowiek cz : ludziki) {
            System.out.println(cz);
        }
    }
}