/*
Zad 1.
Stwórz listę Integerów. Wykonaj zadania:
- dodaj do listy 5 elementów ze scannera
- dodaj do listy 5 elementów losowych
- wypisz elementy
Sprawdź działanie aplikacji
 */


package pl.sda.collections.List.zad1;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        List<Integer> lista = new ArrayList<>();

        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        for (int i = 0; i < 5; i++) {
            int liczba = scanner.nextInt();
            lista.add(liczba);
        }
        for (int i = 0; i < 5; i++) {
            int liczba = random.nextInt(10);
            lista.add(liczba);
        }

        System.out.println(lista);

    }
}
