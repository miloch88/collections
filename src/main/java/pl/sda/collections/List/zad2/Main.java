/*
2.
Stwórz oddzielnego maina, a w nim kolejną listę integerów. Wykonaj zadania:
- dodaj do listy 10 liczb losowych
- oblicz sumę elementów na liście (wypisz ją)
- oblicz średnią elementów na liście (wypisz ją)
- podaj medianę elementów na liście (wypisz ją) (Collections.sort( listaDoPosortowania) -
sortuje listę)
(przykład użycia Collections.sort(lista):
ArrayList<Integer> liczby = new ArrayList<>();
liczby.add(5);
liczby.add(10);
liczby.add(15);
Collections.sort(liczby);
- znajdź największy oraz najmniejszy element na liście (znajdź go posługując się pętlą for)
- wypisz indeks elementu
- znajdź największy oraz najmniejszy element na liście (znajdź go pętlą for, a następnie
określ index posługując się metodą indexOf)
Sprawdź działanie aplikacji
 */

package pl.sda.collections.List.zad2;

import java.util.*;

public class Main {
    public static void main(String[] args) {

        List<Integer> list = new ArrayList<>();
        int suma = 0;

        Random random = new Random();

        for (int i = 0; i < 10; i++) {
            int liczba = random.nextInt(100);
            list.add(liczba);
        }
        List<Integer> listKopia = new ArrayList<>(list);

        System.out.println(list);

        for (int i = 0; i < list.size(); i++) {
            suma += list.get(i);
        }
        System.out.println("Suma wynosi: " + suma);
        System.out.println("Średnia wynosi: " + suma / (double) list.size());

        Collections.sort(list);
        if (list.size() % 2 == 0) {
            double mediana = ((list.get(list.size() / 2)) + (double) (list.get(list.size() / 2 - 1))) / 2;
            System.out.println(mediana);
        } else {
            double mediana = list.get(list.size() / 2);
            System.out.println(mediana);
        }
//        System.out.println(list);
//        System.out.println(listKopia);

        int min = listKopia.get(0), max = listKopia.get(0);

        for (int i = 0; i < listKopia.size(); i++) {
            if (listKopia.get(i) < min) {
                min = listKopia.get(i);
            }
            if (listKopia.get(i) > max) {
                max = listKopia.get(i);
            }
        }


            System.out.printf("Minimum to %d na pozycji %d, a maksymium to %d na pozycji %d", min, listKopia.indexOf(min), max, listKopia.indexOf(max));


    }
}


