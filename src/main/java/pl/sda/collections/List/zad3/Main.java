/*
3. ZANIM ZACZNIESZ REALIZOWAĆ TO ZADANIE ZGŁOŚ SIĘ!
Stwórz oddzielnego maina, a w nim kolejną listę String'ów. Wykonaj zadania:
- dodaj do listy elementy 10030, 3004, 4000, 12355, 12222, 67888, 111200, 225355,
2222, 1111, 3546, 138751, 235912 (jako stringi) (dodaj je posługując się metodą
Arrays.asList())
Przykład użycia Arrays.asList():
ArrayList<Integer> liczby = new ArrayList<>(Arrays.asList(23, 32, 44, 55, 677, 11, 33));
- określ indeks elementu 138751 posługując się metodą indexOf
- sprawdź czy istnieje na liście obiekt 67888 metodą contains (wynik wypisz na ekran)
- sprawdź czy istnieje na liście obiekkt 67889 metodą contains (wynik wypisz na ekran)
- usuń z listy obiekt 67888 oraz 67889 (metoda remove)
- wykonaj ponownie powyższe sprawdzenia.- iteruj całą listę, wypisz elementy na ekran (a. w jednej linii, b. każdy element w
oddzielnej linii).
Sprawdź działanie aplikacji.
 */

package pl.sda.collections.List.zad3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<String> listaLiczb = new ArrayList<>(Arrays.asList("10030", "3004", "4000", "12355", "12222", "67888", "67889",
                "111200", "225355", "2222", "1111", "3546", "138751", "235912"));

        System.out.println(listaLiczb);
        System.out.println("Inteks liczby 138751: " + listaLiczb.indexOf("138751"));
        System.out.println("Obiekt 67888 znajduje się na liście: " + listaLiczb.contains("67888"));
        System.out.println("Obiekt 67888 znajduje się na liście: " + listaLiczb.contains("67889"));

        listaLiczb.removeAll(Arrays.asList("67888","67889"));
//        listaLiczb.remove("67888");
//        listaLiczb.remove("67889");

        System.out.println(listaLiczb);
        for (String s : listaLiczb) {
            System.out.println(s);
        }


    }
}
