/*
4.
Stwórz oddzielnego maina, a w nim kolejną listę String'ów. Wykonaj zadania:
- dodaj do listy losowe 100 elementów z zakresu 0-100. (​konwersja int na string to
String.valueOf())
- oblicz średnią liczb
- usuń wszystkie liczby powyżej średniej
- wypisz wszystkie liczby
- wykonaj kopię listy do tablicy
Sprawdź działanie aplikacji.
 */

package pl.sda.collections.List.zad4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Main {
    public static void main(String[] args) {

        List<String> lista = new ArrayList<>();
        int suma = 0;

        //Wstawiam losowe liczby
        Random random = new Random();
        for (int i = 0; i < 100; i++) {
            int liczba = random.nextInt(100);
            lista.add(String.valueOf(liczba));
        }

        System.out.println(lista);

        //Liczę średnią
        for (String s : lista) {
            int a = Integer.parseInt(s);
            suma += a;
        }
        double srednia = (double) suma / lista.size();
        System.out.println("Srednia liczb: " + srednia);

        //Usuwanie powyżej średniej

        ArrayList<String> powtorzenia = new ArrayList<>();
        for (String s : lista) {
            int a = Integer.parseInt(s);
            if (a > srednia) {
                powtorzenia.add(s);
            }
        }

        lista.removeAll(powtorzenia);

//        for(String s: lista){
//            int a = Integer.parseInt(s);
//            if(a>srednia){
//                lista.remove(s.indexOf(s));
//            }
//        }

        System.out.println(lista);

        String[] tablicaStringow = new String[lista.size()];
        for (int i = 0; i < tablicaStringow.length; i++) {
            tablicaStringow[i] = lista.get(i);
        }

        for (String s : tablicaStringow) {
            System.out.print(s + " ");
        }

    }


}

