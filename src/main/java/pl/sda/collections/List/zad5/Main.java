/*
5. ​Stwórz klasę Student która posiada pola:
nr indeksu
imie
nazwisko
płeć (wartość enum)
Stwórz instancję kolekcji typu ArrayList, która zawiera obiekty klasy Student
w mainie dodaj do kolekcji kilku studentów (dodaj je ręcznie - wpisz cokolwiek).
a. Spróbuj wypisać elementy listy stosując zwykłego "sout'a"
b. Spróbuj wypisać elementy stosując pętlę foreach
c. Wypisz tylko kobiety
d. Wypisz tylko mężczyzn
e. Wypisz tylko indeksy osób
 */

package pl.sda.collections.List.zad5;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<Student> studenci = new ArrayList<>();
        studenci.add(0, new Student("Kamil", "Kamilowski", 0001, Plec.MEZCZYZNA));
        studenci.add(1, new Student("Basia", "Basiowska", 0002,Plec.KOBIETA));
        studenci.add(2, new Student("Michał","Michałowksi", 0003, Plec.MEZCZYZNA));
        studenci.add(3, new Student("Kasia","Kasiowska", 0004,Plec.KOBIETA));

//        for (int i = 0; i < studenci.size() ; i++) {
//            System.out.println(studenci.get(i));
//        }
        // Wypisuje tylko kobiety
        for (Student s: studenci) {
            if(s.plec == Plec.KOBIETA)
            System.out.println(s.toString());
        }
        // Wypisuje tylko mężczyzn
        for(Student s: studenci){
            if(s.plec == Plec.MEZCZYZNA)
                System.out.println(s.toString());
        }

        //Wypisuje tylko indeksy
        for(Student s : studenci){
            System.out.println(s.getIndeks());

        }
    }
}
