package pl.sda.collections.List.zad5;

public class Student {
    String imie;
    String nazwisko;
    int nrIndesku;
    Plec plec;

    public Student(String imie, String nazwisko, int nrIndesku, Plec plec) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.nrIndesku = nrIndesku;
        this.plec = plec;
    }

    @Override
    public String toString() {
        return String.format("Mam na imię %s, mój numer indesku: %04d i jestem %s.", imie,nrIndesku,getPlec());
    }

    private String getPlec() {
        if(plec == Plec.KOBIETA){
            return "kobietą";
        }else{
            return "mężczyzną";
        }
    }

    public String getIndeks(){
        return String.format("Mój numer indeksu: %04d", nrIndesku);
    }
}
