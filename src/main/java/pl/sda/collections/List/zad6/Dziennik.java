package pl.sda.collections.List.zad6;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Dziennik {

    List<Student> listaStudentow;

//    public Dziennik(List<Student> listaStudentow) {
//        this.listaStudentow = listaStudentow;
//    }
//    List<Student> studenci = new ArrayList<>();
//    Dziennik dziennik = new Dziennik(studenci);

    // To powinienem użyć
    public Dziennik() {
        this.listaStudentow = new ArrayList<>();
    }

    public void dodajStudenta(Student student) {
        listaStudentow.add(student);
    }

    public void usunStudenta(Student student) {
        listaStudentow.remove(student);
    }

    public void usunStudenta(String indeks) {
        Student oddstrzal = null;
        for (Student s : listaStudentow) {
            if (s.getNrIndeksu().equals(indeks)) {
                oddstrzal = s;
            }
        }
        listaStudentow.remove(oddstrzal);
    }

    public Student zwrocStudenta(String ideks) {
        for (Student s : listaStudentow) {
            if (s.getNrIndeksu().equals(ideks)) {
                return s;
            }

        }
        return null;
    }

    public Double podajSredniaStudneta(String indeks) {
        double suma = 0;
        for (Student s : listaStudentow) {
            {
                if (s.getNrIndeksu().equals(indeks)) {
                    for (Double d : s.getOcenyStudenta()) {
                        suma += d;
                    }
                    return suma / s.getOcenyStudenta().size();
                }
            }

        }
        return 0.0;
    }

    public List<Student> zagrozeni() {
        List<Student> listaZagrozony = new ArrayList<>();
        try {
            for (Student s : listaStudentow) {
                for (Double d : s.getOcenyStudenta()) {
                    if (d == 2.0) {
                        listaZagrozony.add(s);
                    }
                }
            }
        } catch (NullPointerException e) {
        }
        return listaZagrozony;
    }


}










