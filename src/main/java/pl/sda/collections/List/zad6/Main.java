/*
6.
Stwórz aplikację, a w niej klasę Dziennik. Stwórz również klasę Student. Klasa Student
powinna:
- posiadać listę ocen studenta (List<Double>)
- posiadać (pole) numer indeksu studenta (String)
- posiadać (pole) imię studenta
- posiadać (pole) nazwisko studenta
Dziennik powinna:
- posiadać (jako pole) listę Studentów.
- posiadać metodę 'dodajStudenta(Student):void' do dodawania nowego studenta do
dziennika
- posiadać metodę 'usuńStudenta(Student):void' do usuwania studenta- posiadać metodę 'usuńStudenta(String):void' do usuwania studenta po jego numerze
indexu
- posiadać metodę 'zwróćStudenta(String):Student' która jako parametr przyjmuje numer
indexu studenta, a w wyniku zwraca konkretnego studenta.
- posiadać metodę 'podajŚredniąStudenta(String):double' która przyjmuje indeks studenta
i zwraca średnią ocen studenta.
- posiadać metodę 'podajStudentówZagrożonych():List<Student>'
- posiadać metodę 'posortujStudentówPoIndeksie():List<Student>' - która sortuje listę
studentów po numerach indeksów, a następnie zwraca posortowaną listę.
 */

package pl.sda.collections.List.zad6;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        Dziennik dziennik = new Dziennik();

//        dziennik.dodajStudenta(new Student("0001", "Kasia","Kasiowska"));
        Student student1 = new Student("0006", "Kasia","Kasiowska");
        dziennik.dodajStudenta(student1);
        Student student2 = (new Student("0002","Basia", "Basiowska"));
        dziennik.dodajStudenta(student2);

        dziennik.dodajStudenta(new Student("0005","Jacek", "Jackowski"));
        dziennik.dodajStudenta(new Student("0004","Kamil", "Kamilowski"));
        dziennik.dodajStudenta(new Student("0001","Zosia", "Zosińska"));

        Student student6 = new Student("0003","Wiktoria", "Wiktoriowska");
        dziennik.dodajStudenta(student6);

        student1.setOcenyStudenta(Arrays.asList(3.0, 3.0, 3.0, 4.0));
        student2.setOcenyStudenta(Arrays.asList(3.0,5.0,2.0,3.0));

        System.out.println(dziennik.listaStudentow);

        //Usuwanie studenta
//        dziennik.usunStudenta(new Student("0002","Basia", "Basiowska"));
        //Usuwanie studenta po indeksie
        dziennik.usunStudenta("0006");

        System.out.println(dziennik.listaStudentow);

        //Zwracanie studenta
        System.out.println(dziennik.zwrocStudenta("0004"));

        //średnia ocen;
        System.out.println(dziennik.podajSredniaStudneta("0002"));
        System.out.println();

        //zagrożeni
        System.out.println(dziennik.zagrozeni());

        //sortowanie po indeksie

        System.out.println(dziennik.listaStudentow);
        System.out.println();
//        System.out.println(dziennik.posortuj());


    }
}
