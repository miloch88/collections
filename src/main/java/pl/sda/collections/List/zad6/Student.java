package pl.sda.collections.List.zad6;

import java.util.Collections;
import java.util.List;

public class Student{

    private List<Double> ocenyStudenta;
    private String nrIndeksu;
    private String imie;

    public List<Double> getOcenyStudenta() {
        return ocenyStudenta;
    }

    public void setOcenyStudenta(List<Double> ocenyStudenta) {
        this.ocenyStudenta = ocenyStudenta;
    }

    public void setNrIndeksu(String nrIndeksu) {
        this.nrIndeksu = nrIndeksu;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    private String nazwisko;

    public Student(String nrIndeksu, String imie, String nazwisko) {
        this.nrIndeksu = nrIndeksu;
        this.imie = imie;
        this.nazwisko = nazwisko;
    }

    public String getNrIndeksu() {
        return nrIndeksu;
    }

    @Override
    public String toString() {
        return String.format("Mam na imię: %s i mój numer indeksu to: %s \n", imie, nrIndeksu);
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Student){
            Student ten = (Student) obj;
            if(this.imie == ten.imie && this.nazwisko == ten.nazwisko && this.nrIndeksu == ten.nrIndeksu){
                return true;
            }
        }
        return false;
    }

}

