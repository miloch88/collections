package pl.sda.collections.Maps.zad4;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class MapaTest {
    public static void main(String[] args) {

        Map<String, Integer> hashmapa = new TreeMap<>();
        hashmapa.put("Kamil", 1);
        hashmapa.put("Kasia",2);
        hashmapa.put("Michał",3);
        hashmapa.put("Ania",5);

        System.out.println(hashmapa);

        hashmapa.put("Michał",4);
        System.out.println(hashmapa.get("Kasia"));
        System.out.println(hashmapa);

        System.out.println(hashmapa.keySet());

//        for (String name : names.keySet()){
//            name.get(name);
//        }

    }
}
