package pl.sda.collections.Maps.zad5;

import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String tekst = scanner.nextLine();

        System.out.println(zliczSlowa(tekst));


    }

    private static Map<String, Integer> zliczSlowa(String tekst) {
//
//        String[] words = tekst.split(" ");
//
//        Map<String, Integer> slowo = new HashMap<>();
//
//        for (int i = 0; i < words.length; i++) {
//            int count = 0;
//            for (int j = 0; j < words.length; j++) {
//                if (words[i].equals(words[j])) {
//                    count += 1;
//                }
//            }
//            slowo.put(words[i], count);
//        }
//        return slowo;


        String[] words = tekst.split(" ");

        Map<String, Integer> slowo = new TreeMap<>();

        for (int i = 0; i < words.length; i++) {
            int count = 0;
            for (int j = 0; j < words.length; j++) {
                if (words[i].equals(words[j])) {
                    count += 1;
                }
            }
            slowo.put(words[i], count);
        }
        return slowo;
    }


}
