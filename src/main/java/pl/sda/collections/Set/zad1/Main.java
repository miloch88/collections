package pl.sda.collections.Set.zad1;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Main {
    public static void main(String[] args) {

        Set<Integer> sets = new HashSet<>(Arrays.asList(10,12,10,3,4,12,12,300,12,40,55));

        System.out.println(sets.size());
        Set<Integer> sets1 = new TreeSet<>(sets);
        System.out.println(sets1);

        for (Integer s: sets) {
            System.out.print(s+ " ");
        }

        System.out.println();
        sets.removeAll(Arrays.asList(10,12));

        System.out.println(sets.size());
        System.out.println(sets);

        Set<Integer> sets2 = new TreeSet<>(sets);
        System.out.println(sets2);
    }

}
