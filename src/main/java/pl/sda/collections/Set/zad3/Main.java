package pl.sda.collections.Set.zad3;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {

        ParaLiczb paraLiczb1 = new ParaLiczb(1,2);
        ParaLiczb paraLiczb2 = new ParaLiczb(2,1);
        ParaLiczb paraLiczb3 = new ParaLiczb(1,1);
        ParaLiczb paraLiczb4 = new ParaLiczb(2,2);


        Set<ParaLiczb> pary = new HashSet<>();
        pary.add(paraLiczb1);
        pary.add(paraLiczb2);
        pary.add(paraLiczb3);
        pary.add(paraLiczb4);

        System.out.println(pary);


    }
}
