/*
Utwórz klasę ParaLiczb (int a,int b) i dodaj kilka instancji do zbioru:
{(1,2), (2,1), (1,1), (1,2)}.
Wyświetl wszystkie elementy zbioru na ekran. Czy program działa zgodnie z oczekiwaniem?
 */

package pl.sda.collections.Set.zad3;

public class ParaLiczb {

    public ParaLiczb(int a, int b) {
        this.a = a;
        this.b = b;
    }

    int a;
    int b;

    @Override
    public String toString() {
        return        " Para{" +
                "lewy=" + a +
                ", prawy=" + b +
                '}';
    }
}
