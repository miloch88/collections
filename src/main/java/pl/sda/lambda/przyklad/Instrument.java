package pl.sda.lambda.przyklad;

public interface Instrument {

    void makeSound();
}
