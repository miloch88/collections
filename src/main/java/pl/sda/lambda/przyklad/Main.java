package pl.sda.lambda.przyklad;

public class Main {
    public static void main(String[] args) {

        //klasa anonimowa
        Instrument instrument2 = new Instrument() {
            @Override
            public void makeSound() {System.out.println("Bach bach");}
        };
        instrument2.makeSound();

        //wyrażenie lambda
        Instrument instrument3 = () -> System.out.println("doink");
        instrument3.makeSound();

    }
}
