/*
1. Napisz funkcję, która przyjmuje liczbę i sprawdza, czy liczba ta jest parzysta.
 */

package pl.sda.lambda.zad1;

import java.util.function.Predicate;

public class Main {
    public static void main(String[] args) {

//        Predicate<Integer> liczba = new Predicate<Integer>() {
//            @Override
//            public boolean test(Integer integer) {
//                if(integer%2 == 0){
//
//                return true;
//                }else return false;
//            }
//        };

        Predicate<Integer> liczba = (x)->{
            if (x%2 == 0){
                return true;
            }return false;
        } ;

        System.out.println(liczba.test(12));;

    }
}
