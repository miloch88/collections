/*
2. Napisz funkcję, która przyjmuje listę Stringów i sprawdza, czy wśród nich jest String zawierający słowo "gotchya".
 */

package pl.sda.lambda.zad2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class Main {
    public static void main(String[] args) {


        Predicate<List<String>> lista = (x) -> {
            for (String element : x) {
                if(element.contains("gotchya"))return true;
            }
            return false;
        };

        List<String> zdanie = Arrays.asList("pierwszeijwer;lrewrjsdifoaf;rwerisdfgotchya".split(";"));
        System.out.println(lista.test(zdanie));
    }
}

