/*
3. Napisz klasę person, która ma pola: firstName, lastName, age oraz isMale. Napisz:
	a) funkcję, która zwraca true, jeśli Person jest mężczyzną
	b) funkcję, która zwraca true, jeśli Person ma na imię "Jacek"
	c) funkcję, która zwraca true, jeśli jest osoba jest pełnoletnia.
 */

package pl.sda.lambda.zad3;

import java.util.List;
import java.util.function.Predicate;

public class Main {
    public static void main(String[] args) {

        Person person = new Person("Jacek", "Jackowski", 12, true);
        Person person1 = new Person("Basia", "Basiowska", 52, false);

        Predicate<Person> isMale = (x) -> {
            if (x.isMale) {
                return true;
            }
            return false;
        };

        System.out.println(isMale.test(person));
        System.out.println(isMale.test(person1));

        Predicate<Person> isJacek = x -> x.getFirstName().equals("Jacek");
        System.out.println(isJacek.test(person));
        System.out.println(isJacek.test(person1));

        Predicate<Person> isAdult = x -> x.getAge() >= 18;

//        {
//            if(x.getAge() > 18){
//                return true;
//            }return false;
//        };

        /*
        4.  Napisz funkcję, która zwraca true, jeśli w kolekcji <Person> znajduje się dorosły mężczyzna, który ma na imię jacek.
         */
        System.out.println(isAdult.test(person));
        System.out.println(isAdult.test(person1));

        Predicate<List<Person>> hasAnAdultJacek = x -> {
            for (Person p : x) {
                if (p.getAge() >= 18 && p.getFirstName().toLowerCase().equals("jacek")) {
                    return true;
                }
            }
            return false;
        };

    }
}
