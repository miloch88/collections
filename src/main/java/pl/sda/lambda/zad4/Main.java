/*
Rozgrzewka: napisz funkcję, która przyjmuje listę intów i zwraca listę z samymi liczbami parzystymi
 */

package pl.sda.lambda.zad4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class Main {
    public static void main(String[] args) {

        List<Integer> numbersToCkeck = Arrays.asList(1,2,3,4,5,6,7,8,9,10);

        Function<List<Integer>, List<Integer>>  toEvenNumberList = x -> {
            //nowa lista
            List<Integer> evenNumbers = new ArrayList<>();
            for(int number : x){
                if(number%2 == 0) evenNumbers.add(number);
            }
            return evenNumbers;
        };

        System.out.println(toEvenNumberList.apply(numbersToCkeck));


    }
}
