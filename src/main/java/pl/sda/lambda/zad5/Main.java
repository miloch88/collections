/*
5. Napisz funkcję, która przyjmuje listę <Person> i zwraca listę z samymi kobietami.
 */

package pl.sda.lambda.zad5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class Main {
    public static void main(String[] args) {

        Person person1 = new Person("Basia", 15, true);
        Person person2 = new Person("Tomek", 20, false);
        Person person3 = new Person("Jacek", 25, false);
        Person person4 = new Person("Kasia", 15, true);

        List<Person> listaWszystkich = Arrays.asList(person1, person2, person3, person4);
        System.out.println(listaWszystkich);

        Function<List<Person>, List<Person>> przesianieMezczyzn = x -> {
            List<Person> ListaKobiet = new ArrayList<>();
            for (Person p : listaWszystkich) {
                if (p.plec) ListaKobiet.add(p);
            }
            return ListaKobiet;
        };

        System.out.println(przesianieMezczyzn.apply(listaWszystkich));
    }


}
