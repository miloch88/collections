package pl.sda.lambda.zad5;

import java.awt.*;

public class Person {

    private String imie;
    private int wiek;
    boolean plec;

    public boolean isPlec() {
        return plec;
    }

    public void setPlec(boolean plec) {
        this.plec = plec;
    }

    public Person(String imie, int wiek, boolean plec) {

        this.imie = imie;
        this.wiek = wiek;
        this.plec = plec;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public int getWiek() {
        return wiek;
    }

    public void setWiek(int wiek) {
        this.wiek = wiek;
    }

    public Person(String imie, int wiek) {

        this.imie = imie;
        this.wiek = wiek;
    }

    @Override
    public String toString() {
        return String.format("%s lat: %d", imie, wiek);
    }
}
