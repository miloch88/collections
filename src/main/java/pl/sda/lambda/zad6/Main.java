/*
6. Napisz funkcję, która przyjmuje Person i zwraca Student (stwórz taką klasę. Niech posiada student id),
 jeśli osoba ma mniej niż 18 lat. Student id wygeneruj losowo.
 */


package pl.sda.lambda.zad6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.function.Function;

public class Main {
    public static void main(String[] args) {

        Person person1 = new Person("Basia", 15);
        Person person2 = new Person("Tomek", 20);
        Person person3 = new Person("Jacek", 25);
        Person person4 = new Person("Kasia", 15);

        List<Person> listaLudzi = Arrays.asList(person1, person2, person3, person4);


        Function<Person, Student> mianowanyNaStudenta = x -> {
            if (x.getWiek() < 18) {
                Random random = new Random();
                Student student = new Student(x.getImie(), x.getWiek(), random.nextInt(100));
                return student;
            }
            return null;
        };

/*
7. Napisz funkcję, która przyjmuje listę <Person> i zwraca listę studentów,
korzystając z funkcji z poprzedniego zadania.
 */

        System.out.println(mianowanyNaStudenta.apply(person4));

        Function<List<Person>, List<Student>> tworzenieListyStudentow = x -> {
            List<Student> listaStudnetow = new ArrayList<>();
            for (Person p : listaLudzi) {

                Student student = mianowanyNaStudenta.apply(p);
                if(student != null) listaStudnetow.add(student);
        }
        return listaStudnetow;
    };
        System.out.println(listaLudzi);
        System.out.println(tworzenieListyStudentow.apply(listaLudzi));

}
}
