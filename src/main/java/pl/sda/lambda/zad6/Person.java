package pl.sda.lambda.zad6;

public class Person {

    private String imie;
    private int wiek;

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public int getWiek() {
        return wiek;
    }

    public void setWiek(int wiek) {
        this.wiek = wiek;
    }

    public Person(String imie, int wiek) {

        this.imie = imie;
        this.wiek = wiek;
    }

    @Override
    public String toString() {
        return String.format("%s lat: %d", imie, wiek);
    }
}
