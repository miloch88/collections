package pl.sda.lambda.zad6;

public class Student {

    String imie;
    int wiek;

    int indeks;

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public int getWiek() {
        return wiek;
    }

    public void setWiek(int wiek) {
        this.wiek = wiek;
    }

    public int getIndeks() {
        return indeks;
    }

    public void setIndeks(int indeks) {
        this.indeks = indeks;
    }

    public Student(String imie, int wiek, int indeks) {

        this.imie = imie;
        this.wiek = wiek;
        this.indeks = indeks;
    }

    @Override
    public String toString() {
        return String.format("Nowy student: %s lat: %d, indeks: %d", imie, wiek, indeks);
    }
}
