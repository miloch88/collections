/*
8. Napisz funkcję, która przyjmuje dwie Person i zwraca osobę starszą.
 */

package pl.sda.lambda.zad8;

import java.util.function.BinaryOperator;

public class Main {
    public static void main(String[] args) {

        BinaryOperator<Person> osobaStarsza = (x,y) ->{
            if(x.getWiek()>y.getWiek()){
                return x;
            }
            return y;
        };

        System.out.println(osobaStarsza.apply(new Person("KAsia",12),
                new Person("Kazik",1)));

    }
}

class Person{

    private String imie;
    private int wiek;

    public Person(String imie, int wiek) {
        this.imie = imie;
        this.wiek = wiek;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public int getWiek() {
        return wiek;
    }

    public void setWiek(int wiek) {
        this.wiek = wiek;
    }

    @Override
    public String toString() {
        return imie;
    }
}


