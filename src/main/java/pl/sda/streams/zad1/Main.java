/*
1.Napisz klasę person, która ma pola: firstName, lastName, age oraz isMale.
Mając listę osób i korzystając ze streamów:
a) uzyskaj listę mężczyzn
b) uzyskaj listę dorosłych kobiet
c) uzyskaj Optional<Person> z dorosłym Jackiem
d) uzyskaj listę wszystkich nazwisk osób, które są w przedziale wiekowym: 15-19
e)* uzyskaj sumę wieku wszystkich osób
f)* uzyskaj średnią wieku wszystkich mężczyzn
g)** znajdź nastarszą osobę w liście
 */

package pl.sda.streams.zad1;

import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {

        Person person1 = new Person("Basia", "Basiowska", 15, false);
        Person person2 = new Person("Tomek", "Tomkowski", 55, true);
        Person person3 = new Person("Jacek", "Jackowski", 25, true);
        Person person4 = new Person("Kasia", "Kasiowska", 50, false);
        Person person5 = new Person("Kamil", "Kamilowski", 35, true);
        Person person6 = new Person("Jacek", "Kowalski", 10, true);

        List<Person> people = Arrays.asList(person1, person2, person3, person4, person5);

        //a)
        List<Person> onlyMen = people.stream().filter(x -> x.isMale()).collect(Collectors.toList());
        System.out.println(onlyMen);

        //b)
        List<Person> womenAdult = people.stream().filter(x -> !x.isMale()).filter(x -> x.getAge() >= 18)
                .collect(Collectors.toList());
        System.out.println(womenAdult);

        //c)
        Optional<Person> adultJacek = people.stream().filter(x -> x.getFirstName().toLowerCase().equals("jacek"))
                .filter(x -> x.getAge() >= 18).findAny();
        System.out.println(adultJacek);

        //d)
        //z lewej strony musi być typ który zwracamy;
        List<String> listLastName = people.stream().filter(x -> x.getAge() > 14 && x.getAge() < 20)
                .map(x -> x.getLastName()).collect(Collectors.toList());
        System.out.println(listLastName);

        //e)
        int suma = 0;
        Double wiekWszystkich = people.stream().mapToDouble(x -> x.getAge()).sum();
        System.out.println(wiekWszystkich);
        // Ineger sum = numbers.stream().reduce( 0, (x,y) -> x+y);

        //f)średnia wieku
        OptionalDouble optAfv = people.stream().map(x->x.getAge()).mapToDouble(x->x).average();

        if(optAfv.isPresent()){
        System.out.println(optAfv.getAsDouble());
        }else{
            System.out.println("Przeciez nie ma nikogo na liscie");
        }

        //g)
//        x.getAge()-y.getAge()
//        Comparator.comparingInt(Person::getAge)
//        (x, y) -> Integer.compare(x.getAge(),y.getAge())
        Optional<Person> najstarszaOsoba = people.stream().max((x, y) -> x.getAge()-y.getAge());
        System.out.println(najstarszaOsoba);



    }
}
