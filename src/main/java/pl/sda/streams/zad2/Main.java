/*
2. Stwórz klasę Programmer, która ma klasę Person jako pole. Poza tym, posiada listę języków, którymi się posługuje.
Mogą być one reprezentowane przez klasę String.

Mając listę programistów i korzystając ze streamów:

a) uzyskaj listę programistów, którzy są mężczyznami
b) uzyskaj listę niepełnoletnich programistów (obydwóch płci), którzy piszą w Cobolu
c) uzyskaj listę programistów, którzy znają więcej, niż jeden język programowania
d) uzyskaj listę programistek, które piszą w Javie i Cpp
e) uzyskaj listę męskich imion
f) uzyskaj set wszystkich języków opanowanych przez programistów
g) uzyskaj listę nazwisk programistów, którzy znają więcej, niż dwa języki
h) sprawdź, czy istnieje chociaż jedna osoba, która nie zna żadnego języka
i)* uzyskaj ilość wszystkich języków opanowanych przez programistki
j) uzyskaj średnią ilość opanowanych języków przez programistki
k) uzyskaj płeć programisty, który zna największą ilość języków
l) zwroc listę językow posortowanych wedlug powszechnosci

 */

package pl.sda.streams.zad2;

import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {

        //Lista Person
        Person person1 = new Person("Jacek", "Kowalski", 18, true);
        Person person2 = new Person("Jacek", "Górski", 15, true);
        Person person3 = new Person("Andżelika", "Dżoli", 25, false);
        Person person4 = new Person("Wanda", "Ibanda", 12, false);
        Person person5 = new Person("Marek", "Marecki", 17, true);
        Person person6 = new Person("Johny", "Brawo", 25, true);
        Person person7 = new Person("Stary", "Pan", 80, true);
        Person person8 = new Person("Newbie", "Noob", 12, true);
        Person person9 = new Person("Newbies", "Sister", 19, false);

        List<String> languages1 = Arrays.asList("Java;Cobol;Cpp;Lisp".split(";"));
        List<String> languages2 = Arrays.asList("Java;Lisp".split(";"));
        List<String> languages3 = Arrays.asList("Java;Cobol;Cpp;Lisp;C#".split(";"));
        List<String> languages4 = Arrays.asList("C#;C;Cpp".split(";"));
        List<String> languages5 = Arrays.asList("Java;Assembler;Scala;Cobol;duda;dupa;dupa".split(";"));
        List<String> languages6 = Arrays.asList("Java;Scala".split(";"));
        List<String> languages7 = Arrays.asList("C#;C".split(";"));
        List<String> languages8 = Collections.emptyList();
        List<String> languages9 = Arrays.asList("Java");

        List<List<String>> listaList = Arrays.asList(languages1, languages2, languages3, languages4, languages5
                , languages6, languages7, languages8, languages9);


        Programmer programmer1 = new Programmer(person1, languages1);
        Programmer programmer2 = new Programmer(person2, languages2);
        Programmer programmer3 = new Programmer(person3, languages3);
        Programmer programmer4 = new Programmer(person4, languages4);
        Programmer programmer5 = new Programmer(person5, languages5);
        Programmer programmer6 = new Programmer(person6, languages6);
        Programmer programmer7 = new Programmer(person7, languages7);
        Programmer programmer8 = new Programmer(person8, languages8);
        Programmer programmer9 = new Programmer(person9, languages9);

        List<Programmer> programmers = Arrays.asList(programmer1, programmer2, programmer3, programmer4,
                programmer5, programmer6, programmer7, programmer8, programmer9);

        //a)
        List<Programmer> onlyMen = programmers.stream().filter(x -> x.getPerson().isMale()).collect(Collectors.toList());
        System.out.println(onlyMen);
        System.out.println();

        //b)

//        List<Programmer> notAdultCobolProgrammers = programmers.stream().filter(x->{
//            List<String> lowerCaseLanguages = x.getLanguages().stream().map(y->y.toLowerCase())
//                    .collect(Collectors.toList());
//            return lowerCaseLanguages.contains("cobol");
//
//        }).filter(x->x.getPerson().getAge()<18)
//                .collect(Collectors.toList());

        List<Programmer> youngCobol = programmers.stream().filter(x -> x.getPerson().getAge() < 18).filter(x -> x.getLanguages()
                .contains("Cobol")).collect(Collectors.toList());
        System.out.println(youngCobol);
        System.out.println();

        //c)
        List<Programmer> moreThan3 = programmers.stream().filter(x -> x.getLanguages().size() > 1).collect(Collectors.toList());
        System.out.println(moreThan3);
        System.out.println();

        //d)
        List<Programmer> femaleJavaCpp = programmers.stream().filter(x -> !x.getPerson().isMale())
                .filter(x -> x.getLanguages().contains("Java") && x.getLanguages().contains("Cpp")).collect(Collectors.toList());
        System.out.println(femaleJavaCpp);
        System.out.println();

        //e)
        List<String> maleName = programmers.stream().filter(x -> x.getPerson().isMale())
                .map(x -> x.getPerson().getFirstName()).collect(Collectors.toList());
        System.out.println(maleName);
        System.out.println();

        //f
//        List<String> nameLanguages = listaList.stream().flatMap(x-> x.stream()).collect(Collectors.toList());
//        Set<String> setNameLanguages = new HashSet<>(nameLanguages);

        Set<String> nameLanguages = listaList.stream().flatMap(x -> x.stream()).collect(Collectors.toSet());
        System.out.println(nameLanguages);
        System.out.println();

        //        g) uzyskaj listę nazwisk programistów, którzy znają więcej, niż dwa języki

        List<String> maleLastNameover2 = programmers.stream().filter(x -> x.getPerson().isMale())
                .filter(x -> x.getLanguages().size() > 2).map(x -> x.getPerson().getLastName()).collect(Collectors.toList());
        System.out.println(maleLastNameover2);
        System.out.println();

//        h) sprawdź, czy istnieje chociaż jedna osoba, która nie zna żadnego języka

        Optional<Programmer> osoba = programmers.stream().filter(x -> x.getLanguages().isEmpty()).findAny();
        System.out.println(osoba);
        System.out.println();

//        i)* uzyskaj ilość wszystkich języków opanowanych przez programistki

        Integer ilosc = programmers.stream().filter(x -> !x.getPerson().isMale()).map(x -> x.getLanguages())
                .collect(Collectors.toSet()).size();
        System.out.println(ilosc);
        System.out.println();

//        j) uzyskaj średnią ilość opanowanych języków przez programistki

        OptionalDouble iloscJezykow = programmers.stream().filter(x -> !x.getPerson().isMale())
                .map(x -> x.getLanguages()).mapToDouble(x -> x.size()).average();
        System.out.println("Ilość języków opanowany przez koniety: " + iloscJezykow);
        System.out.println();

//        k) uzyskaj płeć programisty, który zna największą ilość języków
        Optional<Programmer> najzdolniejsza = programmers.stream()
                .max((x,y)-> x.getLanguages().size()-y.getLanguages().size());
        System.out.println(najzdolniejsza.get().getPerson().isMale());


        //optional
//        Optional<Programmer> male = programmers.stream()
//                .max((x, y) -> x.getLanguages().size() - y.getLanguages().size());
//        //ryzykowne: moze nie byc zadnego proramisty w zrodlowej liscie - wtedy poleci exception
//        boolean programmer = programmers.stream()
//                .max((x, y) -> x.getLanguages().size() - y.getLanguages().size())
//                .get().getPerson().isMale();

//        l) zwroc listę językow posortowanych wedlug powszechnosci

    }
}
